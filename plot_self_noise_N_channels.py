import argparse
import os

from matplotlib import pyplot
import numpy as np
from obspy.signal.spectral_estimation import get_nlnm, get_nhnm
import pandas as pd


def read_self_noises(rep_input):
    """
    Read the self noise of all the sensors contained in rep_input and
    also the median of the self noise.
    """
    psd_noises = []
    for file in os.listdir(rep_input):
        if file.endswith('.csv'):
            path_csv = os.path.join(rep_input, file)
            my_csv = pd.read_csv(path_csv, sep=',', header=0)
            psd_noise = my_csv.psd
            if file == 'psd_noise_median.csv':
                psd_noise_50 = psd_noise
            else:
                psd_noises.append(psd_noise)
    freq = my_csv.frequency
    return freq, psd_noises, psd_noise_50


def plot_psd_noise(psd_noises, psd_noise_50, freq, rep_output):
    """
    Plot the psd of the noise for each system and the median psd noise.
    """
    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    cmap = pyplot.get_cmap('jet')
    index_color = np.linspace(0, 255, len(psd_noises) + 1, dtype=np.int16)
    psd_min, psd_max = [], []
    for i, psd_noise in enumerate(psd_noises):
        psd_min.append(np.amin(psd_noise))
        psd_max.append(np.amax(psd_noise))
        ax.semilogx(freq, psd_noise, color=cmap(index_color[i]),
                    alpha=0.4)
    line_noise_50, = ax.semilogx(freq, psd_noise_50,
                                 color=cmap(index_color[-1]))
    period, nlnm = get_nlnm()
    psd_min.append(np.amin(nlnm))
    freq_nhlnm = np.divide(1, period)
    _, nhnm = get_nhnm()
    psd_max.append(np.amax(nhnm))
    line_nlnm, = ax.semilogx(freq_nhlnm, nlnm, color='k')
    line_nhnm, = ax.semilogx(freq_nhlnm, nhnm, color='k')
    ax.legend([line_noise_50, line_nhnm, line_nlnm],
              ['median', 'NHNM', 'NLNM'], loc='best')
    ax.grid()
    ax.set_xlim(freq.min(), freq.max())
    ax.set_ylim(min(psd_min), max(psd_max))
    prop_text = {'weight': 'bold', 'size': 'large'}
    ax.set_xlabel('Frequency (Hz)', prop_text)
    ax.set_ylabel('Noise PSD (dB rel to 1 m**2s**-4/Hz)', prop_text)
    path_figure = os.path.join(rep_output, 'self_noise.png')
    pyplot.savefig(path_figure, dpi=300)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot the self-noise\
                                     of N systems')
    parser.add_argument('rep_input', help='directory containing the csv files.\
                         Each csv file contained the self noise computed for a\
                         channel')
    parser.add_argument('rep_output', help='path directory where the graph\
                         will be stored')
    args = parser.parse_args()
    rep_input = args.rep_input
    rep_output = args.rep_output
    freq, psd_noises, psd_noise_50 = read_self_noises(rep_input)
    plot_psd_noise(psd_noises, psd_noise_50, freq, rep_output)


