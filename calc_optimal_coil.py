#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from matplotlib import pyplot
import numpy as np
from obspy.signal.invsim import corn_freq_2_paz
from obspy.signal.spectral_estimation import get_nlnm, get_nhnm
import pandas
from scipy.optimize import curve_fit
from scipy.signal import freqs_zpk


def calc_geophone_self_noise(f, fc, damping, sensitivity, coil_res):
    # Sensor noise level
    # Havskov & Alguacil, Instrumentation in Earthquake Seismology, 2002
    kB = 1.38064852e-23  # Boltzmann constant
    T = 293  # Temperature in Kelvin
    paz = corn_freq_2_paz(fc, damp=damping)
    w = 2*np.pi*f
    (_, H) = freqs_zpk(paz['zeros'], paz['poles'], paz['gain']*sensitivity,
                       worN=w)
    Nv = 4*kB*T*coil_res  # Voltage noise
    Na = Nv*w**2 / np.absolute(H**2)  # Acceleration noise
    return 10*np.log10(Na)


def _model(params, coil_res):
    sensitivity = params[-1]
    damping = params[-2]
    fc = params[-3]
    f = params[:-3]
    return calc_geophone_self_noise(f, fc, damping, sensitivity, coil_res)


def _csvfile2arrays(file):
    """
    Read quartiles csv file as computed by crosscomparison_ir.
    """
    my_csv = pandas.read_csv(file, sep=',', header=0)
    return (np.array(my_csv.frequency),
            np.array(my_csv.psd))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot on a same figure the \
                                     estimated self noise, and its best fit')
    parser.add_argument('input_file', help='path of the csv file containing \
                        the media self-noise function')
    parser.add_argument('--fmin', help='Calculate best fit for frequencies \
                        contained between fmin and fmax.',
                        type=float, default=0.1)
    parser.add_argument('--fmax', help='Calculate best fit for frequencies \
                        contained between fmin and fmax.',
                        type=float, default=100)
    parser.add_argument('--fc', help='Geophone cutoff frequency.',
                        type=float, default=5.069)
    parser.add_argument('--damp', help='Geophone damping.',
                        type=float, default=0.711132)
    parser.add_argument('--gain', help='Geophone gain.',
                        type=float, default=82.6)
    args = parser.parse_args()

    f, N = _csvfile2arrays(args.input_file)

    imin = np.abs(f-args.fmin).argmin()
    imax = np.abs(f-args.fmax).argmin()

    fig = pyplot.figure(dpi=150)
    fig.suptitle("Self-noise and analog stage impendance estimate")
    ax = fig.gca()
    ax.semilogx(f, N)

    params = np.append(f[imin:imax], np.array([args.fc, args.damp, args.gain]))
    popt, _ = curve_fit(_model, params, N[imin:imax])
    ax.semilogx(f, calc_geophone_self_noise(f,
                                            args.fc, args.damp, args.gain,
                                            popt), label='best fit')
    print("Analog stage global impedance: %d Ohms" % popt)

    (p,  Pl) = get_nlnm()
    (p,  Ph) = get_nhnm()
    f1 = 1./p
    ax.semilogx(f1, Pl, '-.k')
    ax.semilogx(f1, Ph, '-.k', label='N[LH]NM')
    ax.set_xlim(f[0], f[-1])
    ax.grid()
    ax.set_xlabel("Frequency [Hz]")
    ax.set_ylabel("PSD [dB][m**2*s**-4/Hz]")

    pyplot.show()
