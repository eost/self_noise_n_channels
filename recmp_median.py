#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 06:05:51 2020

@author: maxime
"""
import argparse
import numpy as np
import pandas as pd
import os


def read_all_csv(rep_input):
    """
    Read the self noise of all the sensors contained in rep_input and
    also the median of the self noise.
    """
    psd_noises = []
    for file in os.listdir(rep_input):
        if file.endswith('.csv'):
            path_csv = os.path.join(rep_input, file)
            my_csv = pd.read_csv(path_csv, sep=',', header=0)
            if file == 'psd_noise_median.csv':
                pass
            else:
                psd_noises.append(my_csv.psd)
    freq = my_csv.frequency
    return freq, psd_noises


def write_result(frequency, psd_median, rep_output):
    """
    Store the self noise of each channel in a csv file
    and the median of the self noise.
    """
    path_csv = os.path.join(rep_output, 'psd_noise_median.csv')
    data = {'frequency': np.around(frequency, 4),
            'psd': np.around(psd_median, 1)}
    df = pd.DataFrame(data, columns=['frequency', 'psd'])
    df.to_csv(path_csv, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Re Compute the median \
                                     self-noise from csv files')
    parser.add_argument('rep', help='path directory where the csv files are \
                        stored, also used as output path')
    args = parser.parse_args()
    rep = args.rep
    freq, psd_noises = read_all_csv(rep)
    psd_median = np.median(psd_noises, axis=0)
    write_result(freq, psd_median, rep)
