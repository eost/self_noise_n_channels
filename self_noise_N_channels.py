import argparse
import csv
from fractions import gcd
from functools import reduce
import os


from matplotlib import mlab
from matplotlib import pyplot
from matplotlib.mlab import psd, csd
import numpy as np
from obspy import UTCDateTime
from obspy.signal.spectral_estimation import get_nlnm, get_nhnm
from obspy.clients.fdsn import Client
import pandas as pd
from scipy.signal import freqs_zpk
from scipy.special import factorial
import yaml


def read_yaml(path_conf):
    """
    Read the configuration file from the path and return the content.
    """
    with open(path_conf) as conf_yaml:
        conf = yaml.load(conf_yaml)
    return conf


def make_stations_id(path_csv):
    """
    Return a list, each element of the list is a station id.
    """
    with open(path_csv, 'r') as csv_file:
        csv_content = csv.DictReader(csv_file, delimiter=',')
        stations_id = []
        for row in csv_content:
            if row['location_code'] == '__':
                row['location_code'] = ''
            station_id = "{0}.{1}.{2}.{3}".format(row['network'],
                                                  row['station'],
                                                  row['location_code'],
                                                  row['channel'])
            stations_id.append(station_id)
    return stations_id


def extract_stream(conf_extr, stations_id):
    """
    Return a list containing the stream of the N stations.
    Streams are extracted from the FDSN service.
    """
    url_server = conf_extr['url_server']
    start_time = UTCDateTime(conf_extr['starttime'])
    end_time = UTCDateTime(conf_extr['endtime'])
    client = Client(base_url=url_server)
    stream_stations = []
    for sta_id in stations_id:
        print("start waveform extraction: %s" % sta_id)
        stream_sta = client.get_waveforms(*sta_id.split('.'),
                                          start_time, end_time)
        print("Waveform extraction: %s done" % sta_id)
        stream_stations.append(stream_sta)
    return stream_stations


def streams2traces(stream_stations):
    """
    Check that each stream contains only one trace.
    If the stream is composed of multiple trace, it raises a ValueError.
    Return a list containing the corresponding traces.
    """
    trace_stations = []
    for stream_sta in stream_stations:
        if len(stream_sta) != 1:
            msg = "Stream got multiple traces!"
            raise ValueError(msg)
        else:
            trace_sta = stream_sta[0]
        trace_stations.append(trace_sta)
    return trace_stations


def resample_traces(trace_stations):
    """
    Compute the sampling frequency common to all the traces
    (great common divisor to the traces).
    Resample all the traces to this sampling frequency.
    """
    common_fs = reduce(lambda x, y: gcd(x, y),
                       [tr.stats.sampling_rate for tr in trace_stations])
    traces_decimated = []
    for tr in trace_stations:
        factor_decimation = round(tr.stats.sampling_rate / common_fs)
        tr.decimate(factor_decimation)
        traces_decimated.append(tr)
    return traces_decimated, common_fs


def slice_traces(trace_stations, conf):
    """
    Slice traces in order to have all the traces with the same starttime
    and the same endtime.
    """
    starttime = UTCDateTime(conf['param_extract_wf']['starttime'])
    endtime = UTCDateTime(conf['param_extract_wf']['endtime'])
    traces_slice = []
    for trace_sta in trace_stations:
        trace_sta_slice = trace_sta.slice(starttime, endtime)
        traces_slice.append(trace_sta_slice)
    return traces_slice


def process_traces(traces_decimated, conf):
    """
    Slice traces to have traces starting and finishing at the same time.
    Check that all the traces have the same length.
    """
    traces_sliced = slice_traces(traces_decimated, conf)
    if len(set([tr.stats.npts for tr in traces_sliced])) != 1:
        msg = 'The length of the traces is not the same. It may be due the\
               unavailability of the data of one of the sensors.'
        raise Exception(msg)
    return traces_sliced


def cmp_psd_csd_matrix(traces, params):
    """
    Compute the matrix containing the PSD and CSD of all the traces.
    """
    sensor_type = traces[0].stats.channel[1]
    fs = round(traces[0].stats.sampling_rate)
    segment_length_sec = params['segment_length'] * 60
    nbr_sample_seg = segment_length_sec * fs
    percentage_overlapping = params['percentage_overlapping']
    nbr_point_overlap = nbr_sample_seg * percentage_overlapping
    psd_csd_matrix = np.zeros((len(traces), len(traces)), list)
    for i in range(len(traces)):
        for j in range(len(traces)):
            if i == j:
                psd_csd, freq = psd(traces[i].data, NFFT=nbr_sample_seg,
                                    Fs=fs, detrend='mean',
                                    window=mlab.window_hanning,
                                    noverlap=nbr_point_overlap,
                                    sides='default', scale_by_freq=True)
            if i < j:
                psd_csd, _ = csd(traces[i].data, traces[j].data,
                                 NFFT=nbr_sample_seg, Fs=fs,
                                 detrend='mean',
                                 window=mlab.window_hanning,
                                 noverlap=nbr_point_overlap,
                                 sides='default', scale_by_freq=True)
            if sensor_type != 'N':
                    # In case the sensor type is a velocimeter,
                    # convert the PSD and CSD from m²/s²/Hz to m²/s⁴/Hz.
                    # Useful to compare with the N[LH]NM.
                    factor = -np.power(np.multiply(2*np.pi, freq), 2)
                    psd_csd = np.multiply(psd_csd, factor)
            if i > j:
                psd_csd = np.conj(psd_csd_matrix[j, i])
            psd_csd_matrix[i, j] = psd_csd
    return psd_csd_matrix, freq


def str2nbr(str_elem):
    """
    Convert a str in float or in complex number
    """
    if 'j' in str_elem:
        return complex(str_elem)
    elif 'e' in str_elem:
        return float(str_elem)
    else:
        raise ValueError(str_elem)


def parse_paz_from_conf(paz):
    """
    Convert in the good format(complex or float) the poles, zeros,
    and normalization frequency of the sensor.
    """
    dict_prop = {}
    for prop_name, prop_val in paz.items():
        if isinstance(prop_val, list):
            list_conv = []
            for elem in prop_val:
                if isinstance(elem, str):
                    elem_conv = str2nbr(elem)
                elif isinstance(elem, float) or isinstance(elem, int):
                    elem_conv = elem
                else:
                    raise ValueError(elem)
                list_conv.append(elem_conv)
            dict_prop[prop_name] = list_conv
        elif isinstance(prop_val, str):
            prop_val = str2nbr(prop_val)
            dict_prop[prop_name] = prop_val
        else:
            dict_prop[prop_name] = prop_val
    return dict_prop


def parse_tf_from_fdsn(conf, station_id, freq):
    """
    Parse PAZ from FDSN.
    """
    url_server = conf['param_extract_wf']['url_server']
    start_time = UTCDateTime(conf['param_extract_wf']['starttime'])
    end_time = UTCDateTime(conf['param_extract_wf']['endtime'])
    net, sta, loc, chan = station_id.split(".")
    client = Client(url_server)
    inv = client.get_stations(start_time, end_time, network=net,
                              station=sta, location=loc, channel=chan,
                              level="response")
    channel = inv[0][0][0]
    response = channel.response
    tf = response.get_evalresp_response_for_frequencies(freq)
    return tf


def retrieve_tf(conf, station_id, freq):
    """
    Try to read the instrumental response from FDSN if it fails, read the
    instrumental response from the configuration file.
    """
    try:
        tf = parse_tf_from_fdsn(conf, station_id, freq)
        print("Read response from FDSN")
    except Exception:
        paz = parse_paz_from_conf(conf['paz'])
        paz['normalization_factor'] = cmp_normalization_factor(paz)
        print("Read PAZ from the configuration file")
        z, p, k = (paz['zeros'], paz['poles'],
                   paz['normalization_factor']*paz['sensitivity'])
        tf = freqs_zpk(z, p, k, worN=2*np.pi*freq)
    return tf


def cmp_normalization_factor(zeros, poles, fn):
    """
    Computes the normalization factor from the zeros, the poles and the
    normalization frequency.
    """
    num = np.prod(2j*np.pi*fn-poles)
    denom = np.prod(2j * np.pi * fn-zeros)
    normalization_factor = np.absolute(num/denom)
    return normalization_factor


def deconvolve_instr(noise_raw, tf):
    """
    Divide the noise by the square of the module of the instrumental response.
    """
    tf_module = np.abs(tf)
    tf_square = np.multiply(tf_module, tf_module)
    noise_deconv = [np.divide(nr, tf_square) for nr in noise_raw]
    return noise_deconv


def compute_psd_noise(psd_csd_matrix, tf):
    """
    Compute the self-noise of N channels based on the method described
    by Sleeman (Sleeman, 2006, Three-Channel Correlation Analysis:
    A New Technique to Measure Instrumental Noise of Digitizers
    and Seismic Sensors).
    """
    noise_raw = []
    sensor_number = psd_csd_matrix.shape[0]
    for i in range(sensor_number):
        Pii = psd_csd_matrix[i, i][1:]
        factor = 0.5 * np.divide(factorial(sensor_number - 3) * 2,
                                 factorial(sensor_number - 1))
        csd_sum = np.zeros(len(psd_csd_matrix[i, i]) - 1, dtype='complex128')
        for j in range(sensor_number):
            for k in range(sensor_number):
                if k != i and k != j and j != i:
                    csd_product = np.multiply(psd_csd_matrix[j, i][1:],
                                              psd_csd_matrix[i, k][1:])
                    csd_term = np.divide(csd_product, psd_csd_matrix[j, k][1:])
                    csd_sum += csd_term
        Nii = Pii - np.multiply(factor, csd_sum)
        noise_raw.append(Nii)
    noises_deconv = deconvolve_instr(noise_raw, tf[1:])
    psd_noises = [10*np.log10(np.absolute(noise)) for noise in noises_deconv]
    return psd_noises


def plot_psd_noise(psd_noises, freq, legend_graph, rep_output):
    """
    Plot the smoothed psd of the noise for each system.
    """
    cmap = pyplot.get_cmap('jet')
    index_color = np.linspace(0, 255, len(psd_noises) + 1, dtype=np.int16)
    psd_min, psd_max = [], []
    for i, psd_noise in enumerate(psd_noises):
        psd_min.append(np.amin(psd_noise))
        psd_max.append(np.amax(psd_noise))
        pyplot.semilogx(freq, psd_noise, color=cmap(index_color[i]),
                        alpha=0.4)
    period, nlnm = get_nlnm()
    psd_min.append(np.amin(nlnm))
    freq_nhlnm = np.divide(1, period)
    _, nhnm = get_nhnm()
    psd_max.append(np.amax(nhnm))
    pyplot.semilogx(freq_nhlnm, nlnm,
                    color=cmap(index_color[-1]), label='NLNM, NHNM')
    pyplot.semilogx(freq_nhlnm, nhnm, color=cmap(index_color[-1]))
    pyplot.grid()
    pyplot.xlim(freq[0], freq[-1])
    pyplot.ylim(min(psd_min), max(psd_max))
    pyplot.xlabel("Frequency (Hz)")
    pyplot.ylabel("Noise PSD (dB rel to 1 m**2s**-4/Hz)")
    pyplot.legend(loc='best')
    pyplot.title("Instrumental Noise")
    path_figure = os.path.join(rep_output, 'self_noise.png')
    pyplot.savefig(path_figure, dpi=300)


def write_csv(data, path_csv):
    """
    Store data in a csv file.
    """
    df = pd.DataFrame(data, columns=['frequency', 'psd'])
    df.to_csv(path_csv, index=False)


def write_result(frequency, psd_noises, rep_output, stations_name):
    """
    Store the self noise of each channel in a csv file
    and the median of the self noise.
    """
    for i, sta in enumerate(stations_name):
        csv_name = "psd_noise_{0}.csv".format(sta)
        path_csv = os.path.join(rep_output, csv_name)
        data = {'frequency': np.around(frequency, 4),
                'psd': np.around(psd_noises[i], 1)}
        write_csv(data, path_csv)
    path_csv = os.path.join(rep_output, 'psd_noise_median.csv')
    psd_noise_50 = np.percentile(psd_noises, 50, axis=0)
    data = {'frequency': np.around(frequency, 4),
            'psd': np.around(psd_noise_50, 1)}
    write_csv(data, path_csv)


def process(path_conf, csv_stations, rep_output):
    """
    Compute the self-noise of N systems.
    """
    conf = read_yaml(path_conf)
    stations_id = make_stations_id(csv_stations)
    stream_stations = extract_stream(conf['param_extract_wf'], stations_id)
    trace_stations = streams2traces(stream_stations)
    traces_decimated, common_fs = resample_traces(trace_stations)
    traces = process_traces(traces_decimated, conf)
    psd_csd_matrix, freq = cmp_psd_csd_matrix(traces, conf['spectrum'])
    tf = retrieve_tf(conf, stations_id[0], freq)
    psd_noises = compute_psd_noise(psd_csd_matrix, tf)
    stations_name = [trace.get_id() for trace in traces]
    write_result(freq[1:], psd_noises, rep_output, stations_name)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compute the self-noise\
                                     of N systems located closely')
    parser.add_argument('conf_file', help='path configuration file\
                        in yaml format')
    parser.add_argument('csv_stations', help='path csv file containing the\
                        stations id')
    parser.add_argument('rep_output', help='path directory where the results\
                         will be stored')
    args = parser.parse_args()
    path_conf = args.conf_file
    csv_stations = args.csv_stations
    rep_output = args.rep_output
    process(path_conf, csv_stations, rep_output)
