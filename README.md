## Summary:

The program **self_noise_N_channels.py** computes the self noise of N (N >=3) systems (seismometer + digitizer) co-located.
<br>The program implements the algorithm written in the SANDIA report (page 52): **B.John Merchant, Darren M.Hart (2011) Component Evaluation Testing and Analysis Algorithms**.

### Description:
The program get waveforms through a FDSN service.
<br>In the case, the sampling rate of the N systems is different, each waveform is decimated by a certain factor.
This factor is computed from the GCD (Greatest Common Divisor) of the N systems.
<br> Then, cross spectral density and power spectral density are computed on segmented traces to obtain the self noise.
<br> At the end, for each system the PSD of the self noise is saved in a csv file. Moreover, a figure with all the PSD of the self noise is created.

### Usage

The programs needs only 3 mandatories inputs:
* the configuration file in yaml format.
* the csv file containing the stations ID (network, station name, location code and channels).
* the output directory where the results will be stored.

**Example**:

To execute **self_noise_N_channels.py**, the command line is:
```
python3 self_noise_N_channels.py conf_file csv_stations rep_output
```

with:
* conf_file is the path to the configuration file
* csv_stations is the path to the csv file containing the ID of the stations.
* rep_output is the path of the output directory.

![Alt text](/images/self_noise.png?raw=true)

